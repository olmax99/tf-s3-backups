//Global variables
variable "project" {
  type = string
}
variable "slug_version" {
  type = string
}
variable "env" {
  type = string
}
