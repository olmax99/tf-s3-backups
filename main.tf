locals {
  name             = "${var.project}-${var.slug_version}-${var.env}"

  # AWS (dns,s3)
  bucket_name             = "${var.project}-${var.slug_version}-${var.env}-s3-${random_string.name.result}"
  destination_bucket_name = "${var.project}-${var.slug_version}-${var.env}-s3-replica-${random_string.name.result}"
  origin_region           = "eu-central-1"
  replica_region          = "eu-west-1"
# }
}


// Resource random to generate random characters
resource "random_string" "name" { 
  length  = 6
  special = false
  upper   = false
}

