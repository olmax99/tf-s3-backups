terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.24.1"
    }
    random = ">= 2.0"
  }
  required_version = ">= 0.14"
}
