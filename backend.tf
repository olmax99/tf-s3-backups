terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "tfc-explore"

    workspaces {
      name = "tf-s3-backups"
    }
  }
}
